# Ensi\BasketsClient\CalculatorsBasketsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchBasketCalculate**](CalculatorsBasketsApi.md#searchBasketCalculate) | **POST** /baskets/baskets/calculate:search-one | Получить полную информацию о корзине пользователя
[**setBasketCalculatePromoCode**](CalculatorsBasketsApi.md#setBasketCalculatePromoCode) | **POST** /baskets/baskets/calculate:promo-code | Установить промокод



## searchBasketCalculate

> \Ensi\BasketsClient\Dto\SearchBasketCalculateResponse searchBasketCalculate($search_basket_calculate_request)

Получить полную информацию о корзине пользователя

Получить полную информацию о корзине пользователя

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BasketsClient\Api\CalculatorsBasketsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_basket_calculate_request = new \Ensi\BasketsClient\Dto\SearchBasketCalculateRequest(); // \Ensi\BasketsClient\Dto\SearchBasketCalculateRequest | 

try {
    $result = $apiInstance->searchBasketCalculate($search_basket_calculate_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CalculatorsBasketsApi->searchBasketCalculate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_basket_calculate_request** | [**\Ensi\BasketsClient\Dto\SearchBasketCalculateRequest**](../Model/SearchBasketCalculateRequest.md)|  |

### Return type

[**\Ensi\BasketsClient\Dto\SearchBasketCalculateResponse**](../Model/SearchBasketCalculateResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## setBasketCalculatePromoCode

> \Ensi\BasketsClient\Dto\SearchBasketCalculateResponse setBasketCalculatePromoCode($set_basket_promo_code_request)

Установить промокод

Установить промокод

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BasketsClient\Api\CalculatorsBasketsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$set_basket_promo_code_request = new \Ensi\BasketsClient\Dto\SetBasketPromoCodeRequest(); // \Ensi\BasketsClient\Dto\SetBasketPromoCodeRequest | 

try {
    $result = $apiInstance->setBasketCalculatePromoCode($set_basket_promo_code_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CalculatorsBasketsApi->setBasketCalculatePromoCode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **set_basket_promo_code_request** | [**\Ensi\BasketsClient\Dto\SetBasketPromoCodeRequest**](../Model/SetBasketPromoCodeRequest.md)|  |

### Return type

[**\Ensi\BasketsClient\Dto\SearchBasketCalculateResponse**](../Model/SearchBasketCalculateResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

