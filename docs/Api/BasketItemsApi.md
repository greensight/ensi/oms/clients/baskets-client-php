# Ensi\BasketsClient\BasketItemsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchBasketItems**](BasketItemsApi.md#searchBasketItems) | **POST** /baskets/basket-items:search | Search for objects of the type BasketItem



## searchBasketItems

> \Ensi\BasketsClient\Dto\SearchBasketItemsResponse searchBasketItems($search_basket_items_request)

Search for objects of the type BasketItem

Search for objects of the type BasketItem

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BasketsClient\Api\BasketItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_basket_items_request = new \Ensi\BasketsClient\Dto\SearchBasketItemsRequest(); // \Ensi\BasketsClient\Dto\SearchBasketItemsRequest | 

try {
    $result = $apiInstance->searchBasketItems($search_basket_items_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BasketItemsApi->searchBasketItems: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_basket_items_request** | [**\Ensi\BasketsClient\Dto\SearchBasketItemsRequest**](../Model/SearchBasketItemsRequest.md)|  |

### Return type

[**\Ensi\BasketsClient\Dto\SearchBasketItemsResponse**](../Model/SearchBasketItemsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

