# # ProductImage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор картинки | 
**name** | **string** | Наименование (краткое описание картинки) | 
**sort** | **int** | Порядок сортирковки в коллекции картинок | 
**url** | **string** | URL картинки | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


