<?php

namespace Ensi\BasketsClient;

class BasketsClientProvider
{
    /** @var string[] */
    public static $apis = [
        '\Ensi\BasketsClient\Api\CalculatorsBasketsApi',
        '\Ensi\BasketsClient\Api\BasketsApi',
        '\Ensi\BasketsClient\Api\CommonApi',
        '\Ensi\BasketsClient\Api\CustomerBasketsApi',
        '\Ensi\BasketsClient\Api\BasketItemsApi',
    ];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\BasketsClient\Dto\ErrorResponse',
        '\Ensi\BasketsClient\Dto\PatchSettingRequest',
        '\Ensi\BasketsClient\Dto\SearchBasketsResponse',
        '\Ensi\BasketsClient\Dto\Basket',
        '\Ensi\BasketsClient\Dto\SearchBasketCustomerRequest',
        '\Ensi\BasketsClient\Dto\BasketIncludes',
        '\Ensi\BasketsClient\Dto\BasketReadonlyProperties',
        '\Ensi\BasketsClient\Dto\BasketItem',
        '\Ensi\BasketsClient\Dto\SettingFillableProperties',
        '\Ensi\BasketsClient\Dto\SearchBasketCalculateResponse',
        '\Ensi\BasketsClient\Dto\Nameplate',
        '\Ensi\BasketsClient\Dto\EmptyDataResponse',
        '\Ensi\BasketsClient\Dto\SetBasketItemRequest',
        '\Ensi\BasketsClient\Dto\CalculateBasketItems',
        '\Ensi\BasketsClient\Dto\ResponseBodyCursorPagination',
        '\Ensi\BasketsClient\Dto\CalculateBasketItem',
        '\Ensi\BasketsClient\Dto\CalculateBasketAllOf',
        '\Ensi\BasketsClient\Dto\RequestBodyOffsetPagination',
        '\Ensi\BasketsClient\Dto\SetBasketPromoCodeRequest',
        '\Ensi\BasketsClient\Dto\IncludeId',
        '\Ensi\BasketsClient\Dto\ErrorResponse2',
        '\Ensi\BasketsClient\Dto\BasketPromoCodeProperties',
        '\Ensi\BasketsClient\Dto\SetBasketItemRequestItems',
        '\Ensi\BasketsClient\Dto\SearchBasketCalculateRequest',
        '\Ensi\BasketsClient\Dto\SearchBasketsResponseMeta',
        '\Ensi\BasketsClient\Dto\ProductImage',
        '\Ensi\BasketsClient\Dto\RequestBodyPagination',
        '\Ensi\BasketsClient\Dto\Discount',
        '\Ensi\BasketsClient\Dto\Brand',
        '\Ensi\BasketsClient\Dto\PaginationTypeCursorEnum',
        '\Ensi\BasketsClient\Dto\RequestBodyCursorPagination',
        '\Ensi\BasketsClient\Dto\SetBasketPromoCodeRequestAllOf',
        '\Ensi\BasketsClient\Dto\SettingsResponse',
        '\Ensi\BasketsClient\Dto\PaginationTypeOffsetEnum',
        '\Ensi\BasketsClient\Dto\DeleteBasketCustomerRequest',
        '\Ensi\BasketsClient\Dto\CalculateBasket',
        '\Ensi\BasketsClient\Dto\PatchSeveralSettingsRequest',
        '\Ensi\BasketsClient\Dto\SearchBasketItemsResponse',
        '\Ensi\BasketsClient\Dto\Error',
        '\Ensi\BasketsClient\Dto\ResponseBodyOffsetPagination',
        '\Ensi\BasketsClient\Dto\SettingReadonlyProperties',
        '\Ensi\BasketsClient\Dto\SearchBasketsRequest',
        '\Ensi\BasketsClient\Dto\SettingCodeEnum',
        '\Ensi\BasketsClient\Dto\PaginationTypeEnum',
        '\Ensi\BasketsClient\Dto\SearchBasketCustomerResponse',
        '\Ensi\BasketsClient\Dto\Setting',
        '\Ensi\BasketsClient\Dto\ResponseBodyPagination',
        '\Ensi\BasketsClient\Dto\SearchBasketItemsRequest',
        '\Ensi\BasketsClient\Dto\ModelInterface',
    ];

    /** @var string */
    public static $configuration = '\Ensi\BasketsClient\Configuration';
}
